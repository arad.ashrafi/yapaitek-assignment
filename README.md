### A High-Level TODOs List

- [x] Preprocess Data
- [x] Feature Selection
- [x] Fit (a) Supervised Model(s)
- [x] Predict Outcomes for Test Data
      <br>
  <hr>

The first effort in this assignment was to interpret the features. I chose the essential features and preprocessed them by my intuition. Highly correlated features, and features which do not have any reasonable relation with the outcome, e.g., id, were traceable by looking at the columns name. (I do not find working with correlations between features helpful here, and the fact that most of the features were categorical makes finding an accurate correlation between features a little bit harder. So, I trusted my intuition and evaluated the chosen features later by monitoring the model's performance)
Then, one-hot encoding had been applied to the categorical features.

After the preprocessing phase, two supervised models have been chosen for this task. Gradient Boosted Decision Trees and Neural Network have been selected because of their acceptable performance regarding these kinds of problems.

More details about implementations could be found in my comments in the Jupiter notebook.

Finally, both of the base models seem to have perfectly similar behavior. However, if I am to choose between these two models, I will go with Gradient Boosted Decision Trees. It acquired a greater R2 score and learned a better distribution of data.
